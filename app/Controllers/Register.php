<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UserModel;

class Register extends Controller {
    public function index() {
        // ทำงานกับ form ใช้ helper ช่วย
        helper(['form']);
        $data = []; //เก็บ data เป็นค่าว่าง 
        echo view('register', $data);
    }

    public function save() {
        // include helper form
        helper(['form']);
        // set rules validation form
        $rules = [
            'name' => 'required|min_length[3]|max_length[13]',
            'password' => 'required|min_length[6]|max_length[200]',
            'confpassword' => 'matches[password]',
            'Name' => 'required|min_length[1]|max_length[30]',
            'lastname' => 'required|min_length[3]|max_length[30]',
            'School' => 'required|min_length[10]|max_length[100]',
            'dit' => 'required|min_length[3]|max_length[100]',
        ];
        if ($this->validate($rules)) {
            $model = new UserModel();
            $data = [
                'user_name' => $this->request->getVar('name'),
                'Name' => $this->request->getVar('Name'),
                'lastname' => $this->request->getVar('lastname'),
                'School' => $this->request->getVar('School'),
                'dit' => $this->request->getVar('dit'),
                'user_password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
            ];
            $model->save($data);
            return redirect()->to('/login');
        } else {
            $data['validation'] = $this->validator;
            echo view('register', $data);
        }
    }
}