<?php
namespace App\Controllers;
use App\Models\NameProfile;
use CodeIgniter\Controller;

class Profile extends Controller {
    // show names list
    public function index() {
        $Profile = new Profile();
        $users_id = $this->request->getVar('users_id');
        $data['users'] = [
            'Name' => $this->request->getVar('Name'),
            'lastname' => $this->request->getVar('lastname'),
            'School' => $this->request->getVar('School'),
            'dit' => $this->request->getVar('dit')
        ];
        //return view('namelist', $data);
        return view('profile', $data);
        return $this->response->redirect(site_url('/register'));
    }
}