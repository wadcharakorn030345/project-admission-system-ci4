<?php
namespace App\Controllers;
use App\Models\NameModel;
use CodeIgniter\Controller;

class NamesCrud extends Controller {
    // show names list
    public function index() {
        $NameModel = new NameModel();
        $data['course'] = $NameModel->orderBy('C_id', 'ASC')->findAll();
        //return view('namelist', $data);
        return view('namelist', $data);
    }
}