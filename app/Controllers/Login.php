<?php namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\UserModel;

class Login extends Controller {
    public function index() {
        // include helper form
        helper(['form']);
        echo view('login');
    }
    
    

    public function auth() {
        $session = session();
        $model = new UserModel();
        $name = $this->request->getVar('name');
        $password = $this->request->getVar('password');
        $data = $model->where('user_name', $name)->first(); //หาว่ามี name ตรงกับในฐานข้อมูลมั้ย
        if ($data) {
            $pass = $data['user_password'];
            $verify_password = password_verify($password, $pass);
            if ($verify_password) {
                $ses_data = [
                    'user_id' => $data['user_id'],
                    'user_name' => $data['user_name'],
                    'logged_in' => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/dashboard');
            } else {
                $session->setFlashdata('msg', 'กรุณากรอกรหัสผ่าน');
                return redirect()->to('/login');
            }
        } else {
            $session->setFlashdata('msg', 'กรุณากรอกเลขบัตรประชาชน');
            return redirect()->to('/login');
        }
    }

    public function logout() {
        
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }
}