<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ระบบรับสมัครนักศึกษา</title>

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
	<style>
		body {
			background-image: url("img/npru.png");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: 100% 100%;
		}
	</style>
</head>

<body>
	<div class="container" style="text-align: center; border-style: solid; background-color: white; opacity: 0.9;">
	<?php if (isset($_SESSION['username'])) : ?>
		<div class="success"> 
			<h3>
				<?php 
					echo $_SESSION['success'];
					unset($_SESSION['success']);
				?>
			</h3>
		</div>
	<?php endif ?>
		
	<?php if (isset($_SESSION['username'])) : ?>
            <p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
            <p><a href="index.php?logout='1'" style="color: red;">Logout</a></p>
     <?php endif ?>

	 <?php include('header.php'); ?>
	 <br>
	 
		<div class="row">
			<div class="col">
				<img src="/img/11.png" width="1200" height="400">	
			</div>
		</div>
		<br>
		
		
		<div class="container-fluid" style="border-style: solid;">
			

		<br>
		<br>
		<h1>ระบบรับสมัครนักศึกษา</h1>
		<h6>ผ่านเครือข่ายออนไลน์...</h6>
		<br>
		<br>
		<br>
		<a href="/profile"><button type="button" class="btn btn-success">ข้อมูลการสมัคร</button></a>
		<br>
		<br>
		<br>
		<br>

		<a href="/register"><button type="button" class="btn btn-secondary">ลงทะเบียน</button></a> &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<a href="/login"><button type="button" class="btn btn-success">เข้าสู่ระบบ</button></a>
		<br>
		<br>

	</div>
</body>

</html>