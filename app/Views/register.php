<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ลงทะเบียน</title>

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
	<style>
		body {
			background-image: url("img/npru.png");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: 100% 100%;
		}
	</style>
</head>
<body>
<?php include('header.php'); ?>
<div class="container" style="text-align: center; border-style: solid; background-color: white;">
            <br>

            <img src="/img/12.png" width="1200" height="400">

        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <h1>ลงทะเบียน</h1>
                <hr>
                <?php if(isset($validation)): ?>
                    <div class="alert alert-danger"><?= $validation->listErrors(); ?></div>
                <?php endif; ?>
                <form action="/register/save" method="post">
                    <div class="mb-3">
                        <label for="inputname" class="form-label">เลขบัตรประชาชน</label>
                        <input type="text" name="name" class="form-control" id="inputforname" value="<?= set_value('name'); ?>">
                    </div>

                    <div class="mb-3">
                        <label for="inputname" class="form-label">ชื่อ</label>
                        <input type="text" name="Name" class="form-control" id="inputforname" >
                    </div>

                    <div class="mb-3">
                        <label for="inputname" class="form-label">นามสกุล</label>
                        <input type="text" name="lastname" class="form-control" id="inputforname">
                    </div>

                    <div class="mb-3">
                        <label for="inputname" class="form-label">โรงเรียน</label>
                        <input type="text" name="School" class="form-control" id="inputforname">
                    </div>

                    <div class="mb-3">
                        <label for="inputname" class="form-label">จังหวัด</label>
                        <input type="text" name="dit" class="form-control" id="inputforname">
                    </div>
    
                    <div class="mb-3">
                        <label for="inputpassword" class="form-label">รหัสผ่าน</label>
                        <input type="password" name="password" class="form-control" id="inputforpassword">
                    </div>
                    <div class="mb-3">
                        <label for="inputconfpassword" class="form-label">ยืนยันรหัสผ่าาน</label>
                        <input type="password" name="confpassword" class="form-control" id="inputforconfpassword">
                    </div>
                    <button type="submit" class="btn btn-primary">ลงทะเบียน</button>
                </form>
                <hr>
                <a href="/login" class="btn btn-primary">เข้าสู่ระบบ</a>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>