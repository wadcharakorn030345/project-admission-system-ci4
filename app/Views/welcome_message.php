<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ระบบรับสมัครนักศึกษา</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <style>
        body {
            background-image: url("img/npru.png");
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: 100% 100%;
        }
    </style>
</head>

<body>
    <div class="container" style="text-align: center; border-style: solid; background-color: white; opacity: 0.9;">

        <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
            <a class="navbar-brand" href="index.php"><img src="/img/logo.png" height="80 px"></a>
            <div class="container-fluid">
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="/dashboard">หน้าหลัก</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/namelist">สาขาวิชาและจำนวนที่รับสมัคร</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">รายงานการรับสมัคร</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="">ขั้นตอนการสมัคร</a>
                        </li>
                    </ul>

                </div>
            </div>
        </nav>
        <br><br><br><br>

        <div class="row">
            <div class="col">
                <img src="/img/12.png" width="900" height="300">
            </div>
        </div>
        <br>


        <div class="container-fluid" style="border-style: solid;">


            <br>
            <br>
            <h1>ระบบรับสมัครนักศึกษา</h1>
            <h6>ผ่านเครือข่ายออนไลน์...</h6>
            <br>
            <br>
            <br><br><br>

            <a href="/register"><button type="button" class="btn btn-secondary">ลงทะเบียน</button></a> &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="/login"><button type="button" class="btn btn-success">เข้าสู่ระบบ</button></a>

            <br>
            <hr>

            <!--Blog Section
  ============================-->
            <section id="blog" class="padd-section wow fadeInUp">

                <div class="container">
                    <div class="section-title">
                        <i class="fas fa-star"></i>
                        <h4>ข่าวล่าสุด</h4>
                        <div class="pull-right"><a href="https://www.npru.ac.th/news_act_all.php" target="_blank"><i class="fa fa-search"></i> ข่าวทั้งหมด</a> </div>

                    </div>
                    <br>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="block-blog text-left">
                                <a href="http://news.npru.ac.th/u_news/news_detail.php?news_id=30205" target="_blank"><img src="https://news.npru.ac.th/userfiles/PR/nm_images/20221129160902_สนอ. รับตรวจประเมินฯ .jpg" class="img-responsive" alt="img" width="300 px" /></a>
                                <div class="content-blog">
                                    <h5 style="letter-spacing: 0.5px;">
                                        สำนักงานอธิการบดี เข้ารับการตรวจประเมินคุณภาพการศึกษาภายใน ประจำปีการศึกษา 2564 </h5>
                                    <!--<span><i class="fas fa-calendar-alt" style="color:#792024;"></i> 29-11-2565</span> -->
                                    <a class="pull-left readmore" href="http://news.npru.ac.th/u_news/news_detail.php?news_id=30205" target="_blank" style="margin-top:8px;"><i class="fa fa-search"></i> รายละเอียด</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="block-blog text-left">
                                <a href="http://news.npru.ac.th/u_news/news_detail.php?news_id=30187" target="_blank"><img src="https://news.npru.ac.th/userfiles/PR/nm_images/20221128153522_สำนักวิทยบริการฯ อบรมเทคนิคการสืบค้นฯ.jpg" class="img-responsive" alt="img" width="300 px" /></a>
                                <div class="content-blog">
                                    <h5 style="letter-spacing: 0.5px;">
                                        สำนักวิทยบริการฯ จัดอบรม เทคนิคการสืบค้นสื่ออิเล็กทรอนิกส์และฐานข้อมูลออนไลน์ </h5>
                                    <!--<span><i class="fas fa-calendar-alt" style="color:#792024;"></i> 28-11-2565</span> -->
                                    <a class="pull-left readmore" href="http://news.npru.ac.th/u_news/news_detail.php?news_id=30187" target="_blank" style="margin-top:8px;"><i class="fa fa-search"></i> รายละเอียด</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="block-blog text-left">
                                <a href="http://news.npru.ac.th/u_news/news_detail.php?news_id=30163" target="_blank"><img src="https://news.npru.ac.th/userfiles/PR/nm_images/20221125120350_กบ 13 + ลงนามคำรับรองฯ.jpg" class="img-responsive" alt="img" width="300 px" /></a>
                                <div class="content-blog">
                                    <h5 style="letter-spacing: 0.5px;">
                                        มรน. จัดประชุมคณะกรรมการบริหารมหาวิทยาลัย ครั้งที่ 13/2565 และลงนามคำรับรองการปฏิบัติราชการ ประจำปี 2566 </h5>
                                    <!--<span><i class="fas fa-calendar-alt" style="color:#792024;"></i> 25-11-2565</span> -->
                                    <a class="pull-left readmore" href="http://news.npru.ac.th/u_news/news_detail.php?news_id=30163" target="_blank" style="margin-top:8px;"><i class="fa fa-search"></i> รายละเอียด</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="block-blog text-left">
                                <a href="http://news.npru.ac.th/u_news/news_detail.php?news_id=30149" target="_blank"><img src="https://news.npru.ac.th/userfiles/PR/nm_images/20221123134603_OBE รุ่น 2.jpg" class="img-responsive" alt="img" width="300 px" /></a>
                                <div class="content-blog">
                                    <h5 style="letter-spacing: 0.5px;">
                                        หน่วยงานประกันคุณภาพการศึกษา จัดอบรมหลักสูตรตามรูปแบบ Outcome-Based Education (OBE) สู่มาตรฐานสากลด้วยเกณฑ์ AUN-QA (รุ่นที่2) </h5>
                                    <!--<span><i class="fas fa-calendar-alt" style="color:#792024;"></i> 23-11-2565</span> -->
                                    <a class="pull-left readmore" href="http://news.npru.ac.th/u_news/news_detail.php?news_id=30149" target="_blank" style="margin-top:8px;"><i class="fa fa-search"></i> รายละเอียด</a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
            <br>


        </div>
</body>

</html>