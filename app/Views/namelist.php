<!doctype html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Codeigniter Crud</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<body>
<?php include('header.php'); ?>
<div class="container mt-4">
<h1>สาขาและจำนวนที่เปิดรับสมัคร</h1>
<a href="/dashboard" class="btn btn-success">ย้อนกลับ</a>

    <?php
     if(isset($_SESSION['msg'])){
        echo $_SESSION['msg'];
      }
     ?>
  <div class="mt-3">
     <table class="table table-bordered" id="users-list">
       <thead>
          <tr>
             <th>รหัสสาขา</th>
             <th>ชื่อสาขา</th>
             <th>วุติการศึกษา</th>
             <th>จำนวนที่เปิดรับ</th>
          </tr>
       </thead>
       <tbody>
          <?php if($course): ?>
          <?php foreach($course as $course): ?>
          <tr>
             <td><?php echo $course['C_id']; ?></td>
             <td><?php echo $course['Course']; ?></td>
             <td><?php echo $course['Qualification']; ?></td>
             <td><?php echo $course['Amount']; ?></td>
          </tr>
         <?php endforeach; ?>
         <?php endif; ?>
       </tbody>
     </table>
  </div>
</div>
</body>
</html>