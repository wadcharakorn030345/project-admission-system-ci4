<?php 

  
  if (isset($_GET['logout'])) {
    session_destroy();
    //unset($_SESSION['username']);
    header('location: login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <title>หน้าหลัก</title>
    
</head>
<body>
    <!--------------------------------Header----------------------->
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <a class="navbar-brand" href="index.php"><img src="/img/logo.png" height="80 px"></a>
    <div class="container-fluid">
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="/dashboard">หน้าหลัก</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/namelist">สาขาวิชาและจำนวนที่รับสมัคร</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">รายงานการรับสมัคร</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="">ขั้นตอนการสมัคร</a>
          </li>
        </ul>

  

        <form class="d-flex">

            <button class="btn btn-outline-secondary m-1" type="submit"><a href="/register"  style="color:gray; text-decoration: none;"> ลงทะเบียน </a></button>
            <button class="btn btn-outline-success m-1" type="submit"><a href="/login"  style="color:seagreen; text-decoration: none;"> เข้าสู่ระบบ </a></button>
             <?php $session = session(); ?>
                <h5><?php echo "ยินดีต้อนรับ " . $session->get('user_name'); ?></h5>
                <a href="/logout" class="btn btn-danger">Logout</a>
            

            <?php if (isset($_SESSION['username'])) : ?>
              <p> ยินดีต้อนรับ <strong><?php echo $_SESSION['username']; ?> </strong></p>
              <button class="btn btn-outline-danger m-1" type="submit"><a href="index.php?logout='1'" style="color: red;">Logout</a></button>
            <?php endif ?>

          </form>
        
      </div>
    </div>
  </nav>
  <div class="container-fluid">
      <br><br><br><br>
  </div>
    
</body>
</html>