<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ระบบรับสมัครนักศึกษา</title>

	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
	<style>
		body {
			background-image: url("img/npru.png");
			background-repeat: no-repeat;
			background-attachment: fixed;
			background-size: 100% 100%;
		}
	</style>
</head>

<body>
	<div class="container" style=" border-style: solid; background-color: white;">
		<?php if (isset($_SESSION['username'])) : ?>
			<div class="success">
				<h3>
					<?php
					echo $_SESSION['success'];
					unset($_SESSION['success']);
					?>
				</h3>
			</div>
		<?php endif ?>

		<?php if (isset($_SESSION['username'])) : ?>
			<p>Welcome <strong><?php echo $_SESSION['username']; ?></strong></p>
			<p><a href="index.php?logout='1'" style="color: red;">Logout</a></p>
		<?php endif ?>

		<?php include('header.php'); ?>
		<br>

		<div class="row">
			<div class="col">
				<h1 style="text-align: center;">ข้อมูลส่วนตัว</h1>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<p>ชื่อ</p>
						<div class="form-group">
							<input type="text" name="first_name" id="first_name" class="form-control" placeholder="ชื่อ">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<p>นามสกุล</p>
						<div class="form-group">
							<input type="text" name="last_name" id="last_name" class="form-control" placeholder="นามสกุล" tabindex="2">
						</div>
					</div>
				</div>
				<a>เลขประจำตัวประชาชน</a>
				<div class="form-group">
					<input type="text" name="display_name" id="display_name" class="form-control" placeholder="เลขประจำตัวประชาชน" tabindex="3">
				</div>
				<a>จบมาจากโรงเรียน</a>
				<div class="form-group">
					<input type="text" name="display_name" id="display_name" class="form-control" placeholder="โรงเรียน" tabindex="3">
				</div>
				<a>จังหวัด</a>
				<div class="form-group">
					<input type="text" name="display_name" id="display_name" class="form-control" placeholder="จังหวัด" tabindex="3">
				</div>
				<a>เกรดเฉลี่ย</a>
				<div class="form-group">
					<input type="text" name="display_name" id="display_name" class="form-control" placeholder="เกรดเฉลี่ย" tabindex="3">
				</div>
				<a>สาขาที่สมัคร</a>
				<div class="form-group">
					<div class="dropdown">
						<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
							สาขาวิชา
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
							<li><a class="dropdown-item" href="#">วิศวกรรมซอฟต์แวร์ (วท.บ.)</a></li>
							<li><a class="dropdown-item" href="#">วิทยาการข้อมูล (วท.บ.)</a></li>
							<li><a class="dropdown-item" href="#">วิทยาการคอมพิวเตอร์ (วท.บ.)</a></li>
						</ul>
					</div>
				</div>
				<a>ที่อยู่ปัจจุบัน</a>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" name="first_name" id="first_name" class="form-control" placeholder="บ้านเลขที่" tabindex="1">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" name="last_name" id="last_name" class="form-control" placeholder="หมู่ที่" tabindex="2">
						</div>
					</div>
				</div>
				<div class="row">
					<form>
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="province">จังหวัด</label>
								<select name="province_id" id="province" class="form-control">
									<option value="">เลือกจังหวัด</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="amphure">อำเภอ</label>
								<select name="amphure_id" id="amphure" class="form-control">
									<option value="">เลือกอำเภอ</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label for="district">ตำบล</label>
								<select name="district_id" id="district" class="form-control">
									<option value="">เลือกตำบล</option>
								</select>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>

		<script src="assets/jquery.min.js"></script>
		<script src="assets/script.js"></script>
		<a>รหัสไปรษณีย์</a>
		<div class="col-xs-12 col-sm-6 col-md-6">
		<div class="form-group">
			<input type="text" name="last_name" id="last_name" class="form-control" placeholder="รหัสไปรษณีย์" tabindex="2">
		</div></div>
	<a>เบอร์โทรศัพท์</a>
	<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="form-group">
		<input type="text" name="display_name" id="display_name" class="form-control" placeholder="เบอร์โทรศัพท์" tabindex="3">
	</div></div>
	<br>
	<button type="submit" class="btn btn-primary">บันทึก</button>
     <br><br>
	</div>
	</div>
	<br><br>
</body>

</html>